import axios from 'axios';
export const BOOK_ROOM = 'book:bookRoom';

export function bookRoom(info) {
  return {
    type: BOOK_ROOM,
    payload: info
  }
}