import React from 'react';
import ReactDOM from 'react-dom';
import Main from './components/Main';
import { Provider } from "react-redux";
import thunk from 'redux-thunk';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import './style/bootstrap.scss'
// import './style/style.scss'
import reducers from './reducers'


const allReducres = combineReducers(reducers)

const store = createStore(allReducres, applyMiddleware(thunk));

ReactDOM.render(
  <Provider store={store}>
    <div>
      <Main/>
    </div>
  </Provider>
, document.getElementById("app"));