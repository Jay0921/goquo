import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { bookRoom } from '../action/bookAction';

import config from '../config'

class Hotel extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
    this.handleClick = this.handleClick.bind(this);
  }

  componentWillMount() {
    let hotel = Object.keys(this.props.item)[0];

    this.setState({
      hotel: hotel,
      room: this.props.item[hotel]
    })
  }

  handleClick(event) {
    axios.get(`${config.endpoint}/api/v1/room/${event.target.value}`)
      .then((data)=>{
        this.props.onBookRoom(data.data);
      })
  }

  generateRoom() {
    let RoomArr;
    RoomArr = this.state.room.map((item, i)=>
      <div className="row border" key={item._id}>
        <div className="col-8">
          <h6>Room Type: {item.roomTypeLabel}</h6>
          <p>
            Description:
            <br/>
            <small>{item.bedTypeLabel} x {item.bedNumber}</small>
            <br/>
            <small>{item.boardCodeDescription}</small>
          </p>
        </div>
        <div className="col-4">
          <h6>RM {item.totalPrice}</h6>
          <button value={item._id} onClick={this.handleClick}>Book Now!</button>
        </div>
      </div>
      );
    return RoomArr;
  }

  render() {
    return (
      <div>
        <h1>{this.state.hotel}</h1>
        {this.generateRoom()}
      </div>
    )
  }

}

const mapStateToProps = state => {
  return state;
}

const mapActionsToProps = {
  onBookRoom: bookRoom
}

export default connect (mapStateToProps, mapActionsToProps)(Hotel)