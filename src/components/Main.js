import React from 'react';
import axios from 'axios';

import config from '../config'
import Hotel from './Hotel';
import Booking from './Booking';

export default class Main extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    axios.get(`${config.endpoint}/api/v1/hotel`)
    .then((data)=>{
      this.setState({
        data: data.data,
      })
    })
  }

  generateRoom() {
    let HotelArr;
    HotelArr = this.state.data.map((item, a)=>
        <Hotel key={a} item={item}/>
      );

    return HotelArr;
  }

  render() {
    return(
      <div className="container">
        <Booking/>
        {this.state.data ? this.generateRoom() : null }
      </div>
    );
  }
}
