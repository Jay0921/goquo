import React from 'react';

import { connect } from 'react-redux';

class Booking extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  generateDisplay() {
    let booking = this.props.booking;
    if(booking) {
      return (
        <div className="border">
          <div className="row">
            <div className="col">
              <h1>Your Booking</h1>
            </div>
          </div>
          <div className="row">
            <div className="col-8">
              <h6>Hotel: {booking.hotel}</h6>
              <h6>Room Type: {booking.roomTypeLabel}</h6>
              <p>
                Description:
                <br/>
                <small>{booking.bedTypeLabel} x {booking.bedNumber}</small>
                <br/>
                <small>{booking.boardCodeDescription}</small>
              </p>
            </div>
            <div className="col-4">
              <h6>RM {booking.totalPrice}</h6>
            </div>
          </div>
        </div>
        
      )
    }
  }

  render() {
    return (
      <div>
        {this.generateDisplay()}
      </div>
    )
  }

}

const mapStateToProps = state => {
  return state;
}

export default connect (mapStateToProps)(Booking)