const express = require('express');
const Room = require('../../../app/Domain/room');

const router = express.Router();

router.get('/hotel', (req, res, next) => {
  Room
    .getRoomsByHotel()
    .then((rooms)=>{
      res.status(200).send(rooms);
    })
    .catch((error)=>{
      res.status(422).send(error);
    });
});

router.get('/room/:id?', (req, res, next) => {
  let id = req.params.id;
  Room
    .getRoomsById(id)
    .then((rooms)=>{
      res.status(200).send(rooms);
    })
    .catch((error)=>{
      res.status(422).send(error);
    });
});


module.exports = router;