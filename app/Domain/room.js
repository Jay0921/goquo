const Rooms = require('../../db/Rooms');

module.exports = {

  getRooms() {
    return new Promise((resolve, reject) => {
      Rooms
        .find()
        .then((result)=>{
          resolve(result);
        })
        .catch((error)=>{
          reject(error);
        })
    });
  },

  getRoomsByHotel() {
    return new Promise((resolve, reject) => {
      Rooms
        .findByHotel()
        .then((result)=>{
          resolve(result);
        })
        .catch((error)=>{
          reject(error);
        })
    });
  },

  getRoomsById(id) {
    return new Promise((resolve, reject) => {
      Rooms
        .findById(id)
        .then((result)=>{
          resolve(result);
        })
        .catch((error)=>{
          reject(error);
        })
    });
  }
}