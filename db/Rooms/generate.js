const uuidv4 = require('uuid/v4');
const fs = require('fs');

const hotelName = [
  "Bilton",
  "Shangri-ra",
  "Citz-Carlton"
]

const descriptionList = [
  'Wi-Fi',
  'Breakfast',
  'Cancellation policy'
]

const arrRoomTypes = {
  single: {
    numberOfBed: 1,
    bed: 'Single Bed',
    size: '92 cm x 187 cm',
    price: {
      minimum: 70,
      maximum: 100,
    }
  },
  twin: {
    numberOfBed: 2,
    bed: 'Single Bed',
    size: '92 cm x 187 cm',
    price: {
      minimum: 90,
      maximum: 120,
    }
  },
  triple: {
    numberOfBed: 3,
    bed: 'Single Bed',
    size: '92 cm x 187 cm',
    price: {
      minimum: 110,
      maximum: 150,
    }
  },
  double: {
    numberOfBed: 1,
    bed: 'Double Bed',
    size: '137 cm x 187 cm',
    price: {
      minimum: 100,
      maximum: 130,
    }
  },
  queen: {
    numberOfBed: 1,
    bed: 'Queen Bed',
    size: '153 cm x 203 cm',
    price: {
      minimum: 100,
      maximum: 150,
    }
  },
  king: {
    numberOfBed: 1,
    bed: 'King Bed',
    size: '183 cm x 203 cm',
    price: {
      minimum: 170,
      maximum: 200,
    }
  },
  president: {
    numberOfBed: 1,
    bed: 'King Bed',
    size: '183 cm x 203 cm',
    price: {
      minimum: 300,
      maximum: 450,
    }
  }
  
}

let rooms = [];

generateData = (loop) => {
  for (let i = 0; i < loop; i++) {
    let room = {};
    room._id = uuidv4();

    room.hotel = hotelName[Math.floor(Math.random()*hotelName.length)];

    let roomTypes = Object.keys(arrRoomTypes),
        roomType = roomTypes[Math.floor(Math.random()*roomTypes.length)];

    room.roomTypeLabel = roomType;
    room.bedTypeLabel = arrRoomTypes[roomType].bed;
    room.bedNumber = arrRoomTypes[roomType].numberOfBed;
    room.bedSize = arrRoomTypes[roomType].size;
    room.totalPrice = Math.floor(Math.random() * arrRoomTypes[roomType].price.maximum) + arrRoomTypes[roomType].price.minimum;

    room.boardCodeDescription = descriptionList[Math.floor(Math.random()*descriptionList.length)];

    rooms.push(room);
  }
  
  fs.writeFile('./test.js', JSON.stringify(rooms), function(err, data){
    if (err) console.log(err);
    console.log("Successfully Written to File.");
});
};

generateData(10);