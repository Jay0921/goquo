const _ = require('underscore');
const Rooms = require('./data');

module.exports = {

  find() {
    return new Promise((resolve, reject) => {
      resolve(Rooms);
    })
  },

  findByHotel() {
    let hotels = _.keys(_.countBy(Rooms, 'hotel'));
    let rooms = [];

    _.each(hotels,(hotel)=>{
      let temp = {};
      temp[hotel] = _.where(Rooms, {hotel: hotel});
      rooms.push(temp);
    })

    return new Promise((resolve, reject) => {
      resolve(rooms);
    })
  },

  findById(id) {
    var room = _.findWhere(Rooms, {_id: id}) || {};
    return new Promise((resolve, reject) => {
      resolve(room);
    })
  }
}